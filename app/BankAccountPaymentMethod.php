<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccountPaymentMethod extends Model
{
    /**
     * Get the user that owns the bank account payment method.
     */
    public function userAccount()
    {
        return $this->belongsTo('userAccount');
    }
}
