<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCardPaymentMethod extends Model
{
    /**
     * Get the user that owns the credit card payment method.
     */
    public function userAccount()
    {
        return $this->belongsTo('userAccount');
    }
}
