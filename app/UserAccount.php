<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    /**
     * Get the users for the user account.
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Get the addresses for the user account.
     */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    /**
     * Get the bank account payment methods for the user account.
     */
    public function bankAccountPaymentMethods()
    {
        return $this->hasMany(BankAccountPaymentMethod::class);
    }

    /**
     * Get the credit card payment methods for the user account.
     */
    public function creditCardPaymentMethods()
    {
        return $this->hasMany(CreditCardPaymentMethods::class);
    }
}
