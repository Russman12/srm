<?php

namespace App\Http\Controllers;

use App\BankAccountPaymentMethod;
use Illuminate\Http\Request;

class BankAccountPaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BankAccountPaymentMethod  $bankAccountPaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function show(BankAccountPaymentMethod $bankAccountPaymentMethod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BankAccountPaymentMethod  $bankAccountPaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function edit(BankAccountPaymentMethod $bankAccountPaymentMethod)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BankAccountPaymentMethod  $bankAccountPaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BankAccountPaymentMethod $bankAccountPaymentMethod)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BankAccountPaymentMethod  $bankAccountPaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function destroy(BankAccountPaymentMethod $bankAccountPaymentMethod)
    {
        //
    }
}
