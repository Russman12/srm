<?php

namespace App\Http\Controllers;

use App\CreditCardPaymentMethod;
use Illuminate\Http\Request;

class CreditCardPaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CreditCardPaymentMethod  $creditCardPaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function show(CreditCardPaymentMethod $creditCardPaymentMethod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CreditCardPaymentMethod  $creditCardPaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function edit(CreditCardPaymentMethod $creditCardPaymentMethod)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CreditCardPaymentMethod  $creditCardPaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CreditCardPaymentMethod $creditCardPaymentMethod)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CreditCardPaymentMethod  $creditCardPaymentMethod
     * @return \Illuminate\Http\Response
     */
    public function destroy(CreditCardPaymentMethod $creditCardPaymentMethod)
    {
        //
    }
}
