<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the user account that owns the user.
     */
    public function userAccount()
    {
        return $this->belongsTo(UserAccount::class);
    }

    /**
     * The addresses that belong to the user.
     */
    public function addresses()
    {
        return $this->userAccount()->with('addresses');
    }

    /**
     * Get the bank account payment methods for the user account.
     */
    public function bankAccountPaymentMethods()
    {
        return $this->userAccount()->with('bankAccountPaymentMethods');
    }

    /**
     * Get the credit card payment methods for the user account.
     */
    public function creditCardPaymentMethods()
    {
        return $this->userAccount()->with('creditCardPaymentMethods');
    }
}
