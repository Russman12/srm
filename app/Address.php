<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * Get the user account that owns the user.
     */
    public function userAccount()
    {
        return $this->belongsTo(UserAccount::class);
    }

    /**
     * Get the user account that owns the user.
     */
    public function users()
    {
        return $this->userAccount()->with('users');
    }
}
