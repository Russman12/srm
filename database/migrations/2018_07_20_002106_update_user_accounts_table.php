<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_accounts', function (Blueprint $table) {
            $table->unsignedInteger('billing_address_id')->nullable();
            $table->foreign('billing_address_id')->references('id')->on('addresses');
            $table->unsignedInteger('mailing_address_id')->nullable();
            $table->foreign('mailing_address_id')->references('id')->on('addresses');
            $table->unsignedInteger('other_address_id')->nullable();
            $table->foreign('other_address_id')->references('id')->on('addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_accounts', function (Blueprint $table) {
            $table->dropForeign('user_accounts_billing_address_id_foreign');
            $table->dropForeign('user_accounts_mailing_address_id_foreign');
            $table->dropForeign('user_accounts_other_address_id_foreign');
            $table->dropColumn(['billing_address_id', 'mailing_address_id', 'other_address_id']);
        });
    }
}
