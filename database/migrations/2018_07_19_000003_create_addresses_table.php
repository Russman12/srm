<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_account_id');
            $table->foreign('user_account_id')->references('id')->on('user_accounts');
            $table->string('street_1', 255)->nullable();
            $table->string('street_2', 255)->nullable();
            $table->string('city', 63)->nullable();
            $table->string('state', 63)->nullable();
            $table->string('zip', 63)->nullable();
            $table->string('country', 63)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
