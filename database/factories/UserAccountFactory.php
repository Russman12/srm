<?php

use Faker\Generator as Faker;

$factory->define(App\UserAccount::class, function (Faker $faker) {
    return [
        'name' => $faker->lastName(),
        'user_account_type' => $faker->randomElement(['donor', 'recipient', 'admin'])
    ];
});
