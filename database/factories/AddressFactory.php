<?php

use Faker\Generator as Faker;

$factory->define(App\Address::class, function (Faker $faker) {
    return [
        'user_account_id' => $faker->numberBetween(1, 5),
        'street_1' => $faker->buildingNumber . ' ' . $faker->streetName,
        'street_2' => $faker->secondaryAddress,
        'city' => $faker->city,
        'state' => $faker->state,
        'zip' => $faker->postcode,
        'country' => $faker->country,
    ];
});
