<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

define('PASSWORD', Hash::make('testing123'));

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'user_account_id' => $faker->numberBetween(1, 5),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'middle_name' => $faker->firstName,
        'email' => $faker->unique()->safeEmail,
        'password' => PASSWORD, // secret
        'remember_token' => str_random(10),
    ];
});
