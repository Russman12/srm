<?php

use Faker\Generator as Faker;

$factory->define(App\BankAccountPaymentMethod::class, function (Faker $faker) {

    return [
        'name' => $faker->company,
        'bank_account_number' => $faker->bankAccountNumber,
        'routing_number' => $faker->bankRoutingNumber
    ];
});
