<?php

use Faker\Generator as Faker;

$factory->define(App\CreditCardPaymentMethod::class, function (Faker $faker) {

    return [
        'credit_card_number' => $faker->creditCardNumber,
        'expiration_month' => $faker->numberBetween(1, 12),
        'expiration_year' => $faker->numberBetween(18, 30),
        'name_on_card' => $faker->name,
    ];
});
