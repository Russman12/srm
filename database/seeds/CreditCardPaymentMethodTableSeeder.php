<?php

use Illuminate\Database\Seeder;

class CreditCardPaymentMethodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (App\UserAccount::get() as $ua) {
            $ua->bankAccountPaymentMethods()->save(factory(App\CreditCardPaymentMethod::class)->make());
        }
    }
}
