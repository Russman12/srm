<?php

use Illuminate\Database\Seeder;

class BankAccountPaymentMethodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (App\UserAccount::get() as $ua) {
            $ua->bankAccountPaymentMethods()->save(factory(App\BankAccountPaymentMethod::class)->make());
        }
    }
}
